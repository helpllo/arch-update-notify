## arch-update-notify
### What it is?
It is a archlinux update notifer.
Uses dunst, cron and checkupdates from pacman-contrib.
## How to install
```bash
git clone https://gitlab.com/helpllo/arch-update-notify.git 
cd arch-update-notify
g++ main.cc -o run
cp -r ../arch-update-notify ~/.config/dunst/
```
## How to use
Add to user crontab entry:
```
0 15 * * * ~/.config/dunst/arch-update-notify/run
```

